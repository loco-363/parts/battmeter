# ***********************************************
# ***       Loco363 - Parts - BattMeter       ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class BattMeter(generic.bases.LabelPart):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			58.8,
			52.5,
			center=True,
			properties={
				'fill': self.COLOR_UNI,
				'stroke': self.color,
				'stroke-width': 3
			}
		))
		
		# indicator area
		self.add(gen_draw.shapes.Circle(
			c,
			46/2,
			properties={
				'fill': 'white'
			}
		))
	# constructor
# class BattMeter


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(BattMeter(name='BM'), True))
