# ***********************************************
# ***       Loco363 - Parts - BattMeter       ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class BattMeter(generic.bases.Drill):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outline
		self.add(gen_draw.shapes.Rectangle(
			c,
			58.8,
			52.5,
			center=True,
			properties={
				'fill': self.COLOR_UNI
			}
		))
		
		# indicator area
		self.add(gen_draw.shapes.Circle(
			c,
			46/2,
			properties={
				'fill': 'white'
			}
		))
		
		# coords & panel cut-out
		self.add(generic.DrillHole(c, 45, cpos=(0, -5, 'middle')))
		
		# mounting holes
		a = 37.7/2
		b = 37.5/2
		self.add(generic.DrillHole(C(c, ( a,  b)), 4, cpos=(-a+0.1, -7, 'start')))
		self.add(generic.DrillHole(C(c, (-a,  b)), 4, cpos=( a-0.1, -6, 'end')))
		self.add(generic.DrillHole(C(c, ( a, -b)), 4, cpos=(-a+0.1, -6, 'start')))
		self.add(generic.DrillHole(C(c, (-a, -b)), 4, cpos=( a-0.1, -6, 'end')))
	# constructor
# class BattMeter


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(BattMeter(), True))
